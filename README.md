# Wardrobify

Team:

* Ariana - Hats
* Cameron - Shoes

## Design
## Shoes microservice

Using models to gather data into shoes, so that we could make a list encoder to ultimately display them on a react webpage

## Hats microservice

* The Hats API/microservice has a Hat model whose location field is populated by Location instances from the Location model/class in the Wardrobe API/microservice.
* These hats are then displayed on a website built using React.

