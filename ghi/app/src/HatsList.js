function HatsList(props) {

  async function handleDeleteHat(event) {
    // event.preventDefault();
    const buttonValueContainingHatID = event.target.value;

    const hatUrl = 'http://localhost:8090/api/hats/'+buttonValueContainingHatID+"/";
    const fetchConfig = {
      method: "delete",
    };
    const hatPostResponse = await fetch(hatUrl, fetchConfig);
    if (hatPostResponse.ok) {
      // console.log("hat deleted");
      window.location.reload();
    }
  }

  return (
    <div className="container">
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Closet</th>
            <th>Section</th>
            <th>Shelf</th>
            <th>Delete?</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map(hat => {
            return (
              <tr key={hat.href}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style }</td>
                <td>{ hat.color }</td>
                <td><a href={ hat.picture_url }><img src={ hat.picture_url } height="100"/></a></td>
                <td>{ hat.location.closet_name }</td>
                <td>{ hat.location.section_number}</td>
                <td>{ hat.location.shelf_number }</td>
                <td>
                  <button onClick={handleDeleteHat} value={hat.id} className="btn btn-danger">Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default HatsList;

