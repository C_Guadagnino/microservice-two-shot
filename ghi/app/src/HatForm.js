import React from 'react';

class HatForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      fabric: '',
      style: '',
      color: '',
      pictureUrl: '',
      location: '',
      locations: []
    }
    this.handleFabricChange = this.handleFabricChange.bind(this);
    this.handleStyleChange = this.handleStyleChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
    this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    data.picture_url = data.pictureUrl;
    delete data.pictureUrl;
    delete data.locations;
    // console.log(data);

    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const hatPostResponse = await fetch(hatUrl, fetchConfig);
    if (hatPostResponse.ok) {
      const newHat = await hatPostResponse.json();
      console.log(newHat);

      const cleared = {
        fabric: '',
        style: '',
        color: '',
        pictureUrl: '',
        location: '',
      };
      this.setState(cleared);
    }
  }

  handleFabricChange(event) {
    const fabricValue = event.target.value;
    this.setState({fabric: fabricValue});
  }

  handleStyleChange(event) {
    const styleValue = event.target.value;
    this.setState({style: styleValue});
  }

  handleColorChange(event) {
    const colorValue = event.target.value;
    this.setState({color: colorValue});
  }

  handlePictureUrlChange(event) {
    const pictureUrlValue = event.target.value;
    this.setState({pictureUrl: pictureUrlValue});
  }

  handleLocationChange(event) {
    const locationValue = event.target.value;
    this.setState({location: locationValue});
  }

  async componentDidMount() {
    const locationUrl = 'http://localhost:8100/api/locations/';
    const locationResponse = await fetch(locationUrl);

    if (locationResponse.ok) {
      const locationData = await locationResponse.json();
      // console.log(locationData)

      this.setState({locations: locationData.locations});
    }
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Register a new hat</h1>
              <form onSubmit={this.handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                  <input onChange={this.handleFabricChange} value={this.state.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                  <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleStyleChange} value={this.state.style} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                  <label htmlFor="style">Style</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handlePictureUrlChange} value={this.state.pictureUrl} placeholder="Picture Url" required type="url" name="pictureUrl" id="pictureUrl" className="form-control" />
                  <label htmlFor="pictureUrl">Picture Url</label>
                </div>
                <div className="mb-3">
                  <select onChange={this.handleLocationChange} value={this.state.location} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {this.state.locations.map(location => {
                      return (
                        <option key={location.id} value={location.href}>
                          {location.details}
                        </option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HatForm;