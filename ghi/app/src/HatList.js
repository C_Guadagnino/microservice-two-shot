// This is an example of a Class component 'HatList'
// that is equivalent to the function component 'HatsList' on 'HatsList.js'

import React from 'react';

class HatList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deltedHatID: ''
    }
    this.handleDeleteHat = this.handleDeleteHat.bind(this);
  }


  async handleDeleteHat(event) {
      const buttonValueContainingHatID = event.target.value;
      this.setState({deletedHatID: buttonValueContainingHatID});
      // console.log("buttonValueContainingHatID" + buttonValueContainingHatID);

      const hatUrl = 'http://localhost:8090/api/hats/'+buttonValueContainingHatID+"/";
      const fetchConfig = {
          method: "delete",
      };
      const hatPostResponse = await fetch(hatUrl, fetchConfig);
      if (hatPostResponse.ok) {
          // console.log("hat deleted")
          // changing the state hoping it will refresh the v-DOM and show the updated list (without recently deleted hat) but page doesn't auto-refresh
          this.setState({deletedHatID: ''})
          window.location.reload();
      }
  }

  render() {
    return (
      <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Fabric</th>
              <th>Style</th>
              <th>Color</th>
              <th>Picture</th>
              <th>Closet</th>
              <th>Section</th>
              <th>Shelf</th>
              <th>Delete?</th>
            </tr>
          </thead>
          <tbody>
            {this.props.hats.map(hat => {
              return (
                <tr key={hat.href}>
                  <td>{ hat.fabric }</td>
                  <td>{ hat.style }</td>
                  <td>{ hat.color }</td>
                  <td><img src={ hat.picture_url } height="100"/></td>
                  <td>{ hat.location.closet_name }</td>
                  <td>{ hat.location.section_number}</td>
                  <td>{ hat.location.shelf_number }</td>
                  <td>
                    <button onClick={this.handleDeleteHat} value={hat.id} className="btn btn-danger">Delete</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default HatList;