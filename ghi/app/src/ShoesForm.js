import React from 'react';

class ShoesForm extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        manufacturer: '',
        name: '',
        color: '',
        pictureUrl: '',
        bins: [],
      }

    this.ChangeManufacturer = this.ChangeManufacturer.bind(this);
    this.ChangeName = this.ChangeName.bind(this);    
    this.ChangeColor = this.ChangeColor.bind(this);
    this.ChangePictureUrl = this.ChangePictureUrl.bind(this);
    this.ChangeBin = this.ChangeBin.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    }
	
    
    async componentDidMount() {
        const url = "http://localhost:8100/api/bins/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ bins: data.bins })
        }

    }  

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.picture_url = data.pictureUrl;
        delete data.pictureUrl;
        delete data.bins;
        console.log(data)
    
        const binUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(binUrl, fetchConfig);
        if (response.ok) {
          const newBin = await response.json();
          console.log("what bin: ", newBin);
          const cleared = {
            manufacturer: '',
            name: '',
            color: '',
            pictureUrl: '',
            bin: '',
          }
          this.setState(cleared);
        }
      }

    ChangeManufacturer(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value});
    }
    ChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value});
    }
    ChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value});
    }
    ChangePictureUrl(event) {
        const value = event.target.value;
        this.setState({ pictureUrl: value});
    }
    ChangeBin(event) {
        const value = event.target.value;
        this.setState({ bin: value});
    }



    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a pair of shoes</h1>
                        <form onSubmit={this.handleSubmit} id="create-shoe-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.ChangeManufacturer} value={this.state.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                                <label htmlFor="manufacturer">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.ChangeName} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.ChangeColor} value={this.state.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.ChangePictureUrl} value={this.state.pictureUrl} placeholder="Picture URL" required type="textarea" name="image" id="image" className="form-control" />
                                <label htmlFor="picture_url">Picture Url</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.ChangeBin} value={this.state.bin} placeholder="Bin number" required id="bin" name="bin" className="form-select">
                                <option value="">Choose a bin</option>                                
                                {this.state.bins.map(bin => {
                                return (
                                    <option key={bin.id} value={bin.href}>
                                    {bin.closet_name}
                                    </option>
                                );
                              })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                   </div>
                </div>
            </div>
        )
    }
}


export default ShoesForm
