import React from 'react'

function shoesList (props) {

  async function handleDelete(event) {
		event.preventDefault();
    const shoeId = event.target.value
		const url = `http://localhost:8080/api/shoes/${shoeId}`
		const fetchConfig = {
		  method: "delete",
		}
		const response = await fetch(url, fetchConfig);
		console.log(response);
    window.location.reload();
	}

    return (
      <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Name</th>
              <th>Bin</th>
              <th>Picture</th>
              <th>Remove</th>
            </tr>
          </thead>
          <tbody>
            {props.shoes.map(shoe => {
              return (
                <tr key={shoe.id}>
                  <td>{ shoe.manufacturer }</td>
                  <td>{ shoe.name }</td>
                  <td>{ shoe.bin }</td>
                  <td> <img src={shoe.picture_url} alt='null' width="20%" height="20%" /> </td>
                  <td> <button value={shoe.id} onClick={handleDelete} type="button" className='btn btn-danger'>Delete</button></td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
  
  export default shoesList;